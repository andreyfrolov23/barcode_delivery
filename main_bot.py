
import telebot
from telebot import types
from barcode_delivery import barcode_generate

TOKEN = '5847001622:AAGsw3jYF__XUPHBfcLauE12cTUA2l6bEHk'
bot = telebot.TeleBot(TOKEN)

bot.set_my_commands([
    telebot.types.BotCommand("/generate", "Начать генерацию штрих-кодов"),
    telebot.types.BotCommand("/help", "Что умеет бот")
])

keyboard1 = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True).row('Кудряшов В.В.')


@bot.message_handler(commands=['generate'])
def start_message(message):
    bot.send_message(message.chat.id, "выберете ИП", reply_markup=keyboard1)
    bot.register_next_step_handler(message, choose_ip)


def choose_ip(message):
    bot.send_message(message.chat.id, "введите номер поставки", reply_markup=types.ReplyKeyboardRemove())
    bot.register_next_step_handler(message, generate_barcode, message.text)


def generate_barcode(message, ip):
    print(ip, message.text)
    name = barcode_generate(ip, message.text)
    bot.send_message(message.chat.id, name)


bot.polling(none_stop=True, interval=0)
