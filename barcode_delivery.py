import barcode
from barcode.writer import ImageWriter
import requests
import jinja2
import configparser
import os
from PIL import Image

config = configparser.ConfigParser()
config.read("/etc/bot/andrew/barcode/barcode_delivery/config")


def barcode_image(number):
    ean = barcode.get_barcode_class('code128')
    ean_pass = ean('{barcode_pass}'.format(barcode_pass=number), writer=ImageWriter())
    return ean_pass


class Products:
    def __init__(self, barcode_number, name):
        self._barcode = barcode_number
        self._name = name

    def display_name(self):
        return self._name


class Boxes:
    def __init__(self, boxcode, barcode_number, name):
        self.boxcode = boxcode
        self.barcode_number = barcode_number
        self.name = name


def barcode_generate(ip, deliv):
    ent = config['enterpreneur'][ip]
    delivery = int(deliv)
    print(ent, delivery)
    cookies = {
        '_wbauid': ent,  #'6966029861685089443'
        '___wbu': 'eeba0182-e3e4-4751-8791-06ddbaa799f0.1685089445',
        'external-locale': 'ru',
        'wbx-validation-key': 'f4b122b8-fdd2-4884-a86b-55bafaab3c0c',
        'BasketUID': '01308153c6664269b13fab2252086543',
        'x-supplier-id-external': '5ff08ac8-4ad3-4817-a289-dad86940f75b',
        'WBToken': 'Atb4jgPMooLWDMz2ldcMTekoI3nGdZ3m6Luic-n1z8uF8eQmy7cGBCWXWD1xATpzCKuGOSLsOmJs8qELC-2hh2gsZXGMFp_x4TK_8DR3b-zW_QD3D6JElgwQuBCC',
    }

    headers = {
        'Connection': 'keep-alive',
        # 'Cookie': '_wbauid=6966029861685089443; ___wbu=eeba0182-e3e4-4751-8791-06ddbaa799f0.1685089445; external-locale=ru; wbx-validation-key=f4b122b8-fdd2-4884-a86b-55bafaab3c0c; BasketUID=01308153c6664269b13fab2252086543; x-supplier-id-external=5ff08ac8-4ad3-4817-a289-dad86940f75b; WBToken=Atb4jgPMooLWDMz2ldcMTekoI3nGdZ3m6Luic-n1z8uF8eQmy7cGBCWXWD1xATpzCKuGOSLsOmJs8qELC-2hh2gsZXGMFp_x4TK_8DR3b-zW_QD3D6JElgwQuBCC',
        'Origin': 'https://seller.wildberries.ru',
        'Referer': 'https://seller.wildberries.ru/',
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'same-site',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36',
        'accept': '*/*',
        'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
        'content-type': 'application/json',
        'sec-ch-ua': '"Google Chrome";v="119", "Chromium";v="119", "Not?A_Brand";v="24"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
    }

    json_data = [
        {
            'method': 'ListBarcodesBoxes',
            'params': {
                'incomeID': delivery,
            },
            'id': 'json-rpc_19',
            'jsonrpc': '2.0',
        },
    ]

    json_data_pass = {
        'params': {
            'supplyId': delivery,
        },
        'jsonrpc': '2.0',
        'id': 'json-rpc_21',
    }
    json_data_supply = {
        'method': 'SupplyBarcodesImtNames',
        'params': {
            'supplyId': delivery,
        },
        'jsonrpc': '2.0',
        'id': 'json-rpc_12',
    }

    response_supply = requests.post(
        'https://seller-supply.wildberries.ru/ns/sm-supply/supply-manager/api/v1/supply',
        cookies=cookies,
        headers=headers,
        json=json_data_supply,
    )
    response = requests.post(
        'https://seller-supply.wildberries.ru/ns/sm-box/supply-manager/api/v1/box',
        cookies=cookies,
        headers=headers,
        json=json_data,
    )

    response_pass = requests.post(
        'https://seller-supply.wildberries.ru/ns/sm/supply-manager/api/v1/barcode/tRNDetails',
        cookies=cookies,
        headers=headers,
        json=json_data_pass,
    )

    barcode_pass = str(response_pass.json()['result']['details']['trns'][0]['barcode']['barcodePrefix']) + \
                   str(response_pass.json()['result']['details']['trns'][0]['barcode']['barcodeId'])
    print(barcode_pass)
    img_pass = barcode_image(barcode_pass).save('/etc/bot/andrew/barcode/barcode_delivery/ean_pass2')

    name = 'p' + '{num}'.format(num=response_supply.json()['result']['details'][0]['barcode'])
    locals()[name] = Products(response_supply.json()['result']['details'][0]['barcode'],
             response_supply.json()['result']['details'][0]['imtName'])

    boxes = []
    i = 0
    for box in response.json()[0]['result']['boxes']:
        name = 'p' + '{n}'.format(n=box['barcodes'][0]['barcode'])
        img = barcode_image(box['boxcode']).save('images/{i}'.format(i=i))
        # img = Image.open('img.png')
        i_box = Boxes(img, img_pass, locals()[name].display_name()) #barcode_image(box['boxcode']); box['barcodes'][0]['barcode']
        boxes.append(i_box)
        i += 1

    # print(boxes)
    # for i_box in boxes:
    #     print(i_box.boxcode, i_box.name, i_box.barcode_number)

    all_boxes = [boxes[i:i+3] for i in range(0, len(boxes), 3)]

    template = jinja2.Template("""
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Title</title>
    </head>
    <body>
        <table>
            <tbody>
                {% for gr_box in all_boxes %}
                    <tr>
                    {% for i_box in gr_box %}
                        <td>
                            <img src="{{ i_box.boxcode }}"><br/>
                            <img src="{{ i_box.barcode_number }}"><br/>
                            {{ i_box.name }}
                        </td>
                    {% endfor %}
                    </tr>
                {% endfor %}
            </tbody>
        </table>
    </body>
    </html>
    """)

    html = template.render(all_boxes=all_boxes)
    # print(html)
    site = str(delivery) + '.html'
    with open('/var/www/html/{site}'.format(site=site), 'wb') as f:  #locals()[site]
        f.write(html.encode('utf-8'))
    return site


# <!--            {% for ident_box in gr_box %}
#                 <td>
#                 <img src="{{ ident_box.boxcode }}" > <br/>
#                 <img src="{{ ident_box.barcode_number }}" > <br/>
#                 {{ ident_box.name }}
#                 </td>
#             {% endfor %} -->

# { %
# for gr_box in all_boxes %}
#
# { % endfor %}